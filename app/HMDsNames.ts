export const HMDsNames = {
    LenovoDayDream: "Lenovo Mirage Solo",

    OculusQuest: "Oculus Quest", // Oculus Quest
    OculusRift: "Oculus VR HMD",
    OculusGo: "Oculus Go",

    GearVR: "Gear VR",
    HTCVive: "OpenVR HMD",
    WMR: "WindowsMixedRealityHMD" // or (Acer Mixed Reality) or (WindowsMR)
}