import { Vector3, Quaternion, Vector2 } from "three"

export enum DOF { dof6, dof3, none }
export enum Hand { left = 0, right = 1, touchpad = 2 }

/** Handy native gamepad data wrapper, serialize data into three.js types */
export class VRGamepadData {
    private base: Gamepad;

    public OffsetPos = new Vector3();
    public OffsetRot = new Quaternion();


    public get Base() { return this.base }
    public set Base(v: Gamepad) { this.base = v }

    constructor(gp?: Gamepad) {
        this.base = gp;
    }

    public get Pos() {
        if(this.base.pose.position !== null)
        return new Vector3(
            this.base.pose.position[0],
            this.base.pose.position[1],
            this.base.pose.position[2]
        ).add(this.OffsetPos);
        else return new Vector3(0,0,0).add(this.OffsetPos);
    }
    public get Quat() {
        if(this.base.pose.orientation !== null)
        return new Quaternion(
            this.base.pose.orientation[0],
            this.base.pose.orientation[1],
            this.base.pose.orientation[2],
            this.base.pose.orientation[3]
        ).multiply(this.OffsetRot);
        else return new Quaternion(0,0,0,0).multiply(this.OffsetRot);
    }

    public get HasPos() { return this.base.pose.hasPosition }
    public get HasQuat() { return this.base.pose.hasOrientation }

    public get Touches() {
        let vecs: Vector2[] = [];
        for (let i = 0; i < this.base.axes.length; i += 2) {
            vecs.push(new Vector2(
                this.base.axes[i],
                this.base.axes[i + 1]
            ))
        }
        return vecs;
    }

    public get Buttons() { return this.base.buttons }
    public get IsAnyButtonPressed() {
        for (let i = 0; i < this.Buttons.length; i++)
            if (this.Buttons[i].pressed)
                return true;
        return false;
    }
    public get IsAnyButtonTouched() {
        for (let i = 0; i < this.Buttons.length; i++)
            if (this.Buttons[i].touched)
                return true;
        return false;
    }
    public get Connected() { return this.base.connected }
    public get Dof() {
        if (this.base.pose === null || this.base.pose === undefined)
            return DOF.none;

        if (this.base.pose.hasOrientation && this.base.pose.hasPosition)
            return DOF.dof6;

        if (this.base.pose.hasOrientation)
            return DOF.dof3;
    }
    public get Hand() {
        if (this.base.hand === "left")
            return Hand.left;

        if (this.base.hand === "right")
            return Hand.right;

        return Hand.touchpad;
    }
    public get Id() { return this.base.id }
    public get Index() { return this.base.index }
}
