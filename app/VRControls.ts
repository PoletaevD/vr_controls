import { VRGamepadData, Hand } from "./VRGamepadData";
import { Matrix4, Vector3, Quaternion } from "three";

/** Getting native gamepads and serialzie its into handy wrappers */
export class VRControls {
    private static gpwraps: VRGamepadData[] = [];


    public static get Gamepads() { return this.gpwraps }

    /**
     * Updates data of gamepads
     */
    public static Update(standMatx: Matrix4) {
        let standPos = new Vector3().setFromMatrixPosition(standMatx);
        let standRot = new Quaternion().setFromRotationMatrix(standMatx);
        let gps = navigator.getGamepads();
        this.gpwraps = [];
        for (let i = 0; i < gps.length; i++) {
            let gp = gps[i];
            if (gp !== undefined && gp !== null) {
                let vgp = new VRGamepadData(gp);
                vgp.OffsetPos = standPos;
                vgp.OffsetRot = standRot;
                this.gpwraps.push(vgp);
            }
        }
    }

    /** Get one of collected gamepads by hand side */
    public static GetByHand(hand: Hand) {
        for (let i = 0; i < this.gpwraps.length; i++) {
            if (this.gpwraps[i].Hand === hand)
                return this.gpwraps[i];
        }
    }
}
