const fs = require("fs-extra");
const path = require("path");

let moves = [];

moves = [
    { from: path.join(__dirname, "app", "models"), to: path.join(__dirname, "lib") }
];

for (let i = 0; i < moves.length; i++) {
    fs.copySync(moves[i].from, moves[i].to);
    console.log(`copied from ${moves[i].from} --> ${moves[i].to}`)
}
